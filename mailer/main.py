from utilities.prompt import PromptUtil as Prompt
from utilities.tools import ConsoleTools
from utilities.menu import MenuManager
import requests


class Mailer():

    def __init__(self):
        self.menu = MenuManager()
        self.prompt = Prompt('mailer')
        self.tools = ConsoleTools()


    def start(self):
        while True:
            self.tools.clear_console()
            print("MAILER\n")

            self.menu.show_list("provider",
                [
                    '\t1 - guerillamail.com',
                    '\te - exit'
                ]
            )
            choice = self.prompt.use()

            if choice in ['1', 'guerillamail.com']:
                resp = requests.get('http://api.guerrillamail.com/ajax.php?f=get_email_address&ip=127.0.0.1&agent=Mozilla_foo_bar')
                print(resp.content.decode())
                input("\n\nPress Enter to continue...")

            elif choice in ['e', 'exit']:
                self.tools.clear_console()
                break