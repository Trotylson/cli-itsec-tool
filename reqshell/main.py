import utilities.menu as Menu
from reqshell.libs.request import Request
import json
import os
import re
from utilities.prompt import PromptUtil as Prompt
from utilities.tools import ConsoleTools
from reqshell.libs.reactor import ParamsReactor


class Reqshell():
    def __init__(self):
        # my_path = os.path.dirname(os.path.abspath(__file__))
        self.localhost = os.name[1]

        self.menu = Menu.MenuManager()
        self.req = Request()
        self.reactor = ParamsReactor()
        # session = PromptSession()
        self.prompt = Prompt('reqshell')
        self.tools = ConsoleTools()



    # def clear_shell(self):
    #     os.system('clear')

    def print_help(self):
        # print("\nhelp:\n")
        self.menu.show_list("help", 
            [
            '\thelp - shows this help menu',
            '\tparams - shows actual parameters status',
            '\tset <param_name> = <value> - sets a parameter value',
            '\tmanual, man - print manual (in build)',
            '\tshot, start - send request', 
            '\tclear - clear shell',
            '\texit, back - exit subshell',
            '\tquit, bye - quit tool',
            '\theader - shows response headers',
            '\tcontent - shows response content'
            ])
    
    def print_params(self):
        self.menu.show_menu(self.reactor.params)
        print()
        self.menu.show_response_params(self.reactor.resp_params)


    def start(self):
        self.tools.clear_console()
        self.print_help()
        # print("Type 'manual' to see tool guide or 'help' to see a short guide.")
        while True:
            choice = self.prompt.use()
            print()

            if 'set' in choice:
                try:
                    # print(f"set {_re[0][1]} {_re[0][2]}")
                    _re = re.findall(r'^(set) (.*) = (.*)',choice)
                    # option = _re[0][0]
                    key = _re[0][1]
                    value = _re[0][2]
                    if key in self.reactor.params.keys():
                        self.reactor.params[key] = value
                    self.tools.clear_console()
                    print("\nExample usage: set url = http://google.com")
                    print()
                    self.print_params()
                except Exception as e:
                    print(f"\nBAD COMMAND: {e}\nEXAMPLE INPUT: set url = http://google.com")

            elif choice in ('header', 'headers'):
                self.menu.change_response_params_value(self.reactor.resp_params, 'header')
                self.tools.clear_console()
                print("\nExample usage: set url = http://google.com")
                print()
                self.print_params()

            elif choice in ('content',):
                self.menu.change_response_params_value(self.reactor.resp_params, 'content')
                self.tools.clear_console()
                print("\nExample usage: set url = http://google.com")
                print()
                self.print_params()
            
            elif choice in ('param', 'params'):
                self.tools.clear_console()
                print("\nExample usage: set url = http://google.com")
                print()
                self.print_params()

            elif choice in ('help',):
                self.tools.clear_console()
                self.print_help()

            elif choice in ('clear',):
                self.tools.clear_console()

            elif choice in ('man', 'manual'):
                self.tools.clear_console()
                print("MANUAL")

            elif choice in ('shot', 'start'):
                print("starting request")
                info = self.req.make(url=self.reactor.params['url'], port=self.reactor.params['port'], method=self.reactor.params['method'], headers=self.reactor.params['header'], data=self.reactor.params['body'])
                try:
                    self.tools.clear_console()
                    try:
                        status = info.status_code
                    except:
                        status = f"None Type \n{info}"
                    print(f"STATUS CODE:    {status}")
                    if self.reactor.resp_params['header']:
                        print(f"\nHEADER:\n{json.dumps(dict(info.headers), indent=4)}")
                    if self.reactor.resp_params['content']:
                        try:
                            print(f"\nCONTENT:\n{info.content.decode()}")
                        except UnicodeDecodeError:
                            try:
                                print(f"\nCONTENT:\n{info.content.decode('ASCII')}")
                            except UnicodeDecodeError:
                                with open('a_file', 'wb') as f:
                                    f.write(info.content)
                                print(f"\nCONTENT:\nBinary downloaded as 'a_file'")
                except Exception as e:
                    print(e)
                # input("Press any key to continue...")

            elif choice in ('exit', 'back'):
                # clear_shell()
                # exit()
                self.tools.clear_console()
                break

            elif choice in ('quit', 'bye'):
                self.tools.clear_console()
                exit()
