import json
import requests

class Request():
    def __init__(self):
        pass

    def make(self, url, method="GET", port=None, headers=None,data=None):
        if headers:
            headers = headers.replace("'", "\"")
            headers = json.loads(headers)
        if data:
            data = data.replace("'", "\"")
            data = json.loads(data)
        if port:
            url = url + ":" + str(port)
        try:
            return requests.request(method, url, headers=headers, data=data)
        except Exception as e:
            print(e)