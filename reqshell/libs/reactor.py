

class ParamsReactor():
    def __init__(self):
        self.params = {
            'url': '',
            'port': '',
            'method': 'GET',
            'header': '',
            'body': ''
        }

        self.resp_params = {
            'header': False,
            'content': False
        }

    def __del__(self):
        pass