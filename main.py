from utilities.prompt import PromptUtil as Prompt
from utilities.tools import ConsoleTools
from utilities.menu import MenuManager
from utilities.banners import Banner
from reqshell.main import Reqshell as reqshell
from remote_shell.main import RemoteShell as remote_shell
from mailer.main import Mailer as mailer


menu = MenuManager()
prompt = Prompt('tools')
tools = ConsoleTools()
banner = Banner()

while True:
    tools.clear_console()
    print(banner.main)

    menu.show_list("tools",
        [
            '\t1 - reqshell',
            '\t2 - remote shell',
            '\t3 - mailer',
            '\te - exit'
        ]
    )
    choice = prompt.use()

    if choice in ('1', 'reqshell'):
        reqshell().start()

    elif choice in ('2','remote shell', 'remoteshell', 'shell', 'remote'):
        remote_shell().start()

    elif choice in ('3','mailer'):
        mailer().start()

    elif choice in ('e', 'exit'):
        tools.clear_console()
        exit()