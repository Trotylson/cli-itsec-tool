from utilities.tool_info import ToolInfo

class Banner():
    def __init__(self):
        self.tool_info = ToolInfo()
        self.main = f"""

         $       |   {self.tool_info.name}
        $ $      |
       $$ $$     |   author: {self.tool_info.authon}
       $$ $$     |   vesion: {self.tool_info.version}
        $ $      |   web:    {self.tool_info.web}
         $       |
            """