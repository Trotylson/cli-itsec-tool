from prompt_toolkit import prompt, PromptSession
from prompt_toolkit.styles import Style

class PromptUtil():
    def __init__(self, session_name):
        """
        :param session_name: the name of the subscript
        """
        self.session_name = session_name
        self.session = PromptSession()
        self.style = Style.from_dict({
        # User input (default text).
        '':          '',

        # Prompt.
        'username': 'ansicyan',
        'at':       'ansicyan',
        'host':    'ansicyan',
        'pound':    'ansicyan'
        })

        self.message = [
            ('class:username', '\n\n[anon'),
            ('class:at',       '@'),
            ('class:host',     f'{self.session_name}]'),
            ('class:pound',    '$ ')
        ]

    def use(self):
        return self.session.prompt(self.message, style=self.style)