

class MenuManager():
    def __init__(self):
        pass

    def show_menu(self, menu):
        for key, value in menu.items():
            print(f"|> {key.upper()}:   {value}")

    def show_list(self, topic, items_list):
        print(f"\n {topic}:\n --------------------")
        for item in items_list:
            print(item)

    def show_response_params(self, params):
        for key, value in params.items():
            show = 'No'
            if value == True:
                show = 'Yes'
            print(f"- {key.upper()}:   {show}")

    def change_response_params_value(self, params, _value):
        if params[_value] == False:
            params[_value] = True
        else:
            params[_value] = False