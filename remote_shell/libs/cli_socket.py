from utilities.prompt import PromptUtil as Prompt
import socket

class Socket():
    def __init__(self, host, port: int, buffersize: int):
        self.HOST = host
        self.PORT = port
        self.buffsize = buffersize

    def listening(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            try:
                s.bind((str(self.HOST), int(self.PORT)))
            except socket.error as e:
                print(f"socket error: {e}")
                return
            s.listen()
            try:
                conn, addr = s.accept()
            except KeyboardInterrupt:
                print("\nConnection closed by user.")
                return
            except Exception as e:
                print(f"accept error: {e}")
                return
            with conn:
                print(f"Connected by {addr}")
                prompt = Prompt(addr[0])
                data = conn.recv(int(self.buffsize))
                print(data.decode())
                while True:
                    try:
                        msg = prompt.use()
                        if msg:
                            conn.sendall(bytes(msg, 'utf8'))
                            data = conn.recv(4096)
                            print(data.decode())
                    except KeyboardInterrupt:
                        conn.close()
                        print("Connection closed by user.")
                        # exit()
                        break
                    except Exception as e:
                        print(f"Exception ocure: {e}")
                        continue
                    


    def __del__(self):
        pass