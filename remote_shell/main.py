from utilities.prompt import PromptUtil as Prompt
from utilities.tools import ConsoleTools
import remote_shell.libs.cli_socket as _socket
import utilities.menu as Menu
import re


class RemoteShell():
    def __init__(self):
        self.server_params = {
            'server_host': 'localhost',
            'server_port': '8080',
            'server_buffersize': '1024',
        }

        self.client_params = {
            'client_host': '',
            'client_port': self.server_params['server_port'],
            'client_buffersize': self.server_params['server_buffersize'],
            'payload_dir': 'client_payload.py'
        }

        self.menu = Menu.MenuManager()
        self.prompt = Prompt('remote')
        self.tools = ConsoleTools()

    def print_help(self):
        self.menu.show_list("help", 
            [
            '\thelp - shows this help menu',
            '\tparams - shows actual parameters status',
            '\tset <param_name> = <value> - sets a parameter value',
            '\tmanual, man - print manual (in build)',
            '\tlisten, start - start listening', 
            '\tpayload - create payload file',
            '\tclear - clear shell',
            '\texit, back - exit subshell',
            '\tquit, bye - quit tool'
            ])
        
    def print_params(self):
        self.menu.show_menu(self.server_params)
        print()
        self.menu.show_menu(self.client_params)
        print()

    def start(self):
        self.tools.clear_console()
        self.print_help()
        while True:
            choice = self.prompt.use()
            print()

            if 'set' in choice:
                try:
                    _re = re.findall(r'^(set) (.*) = (.*)',choice)
                    key = _re[0][1]
                    value = _re[0][2]
                    print(key, value)
                    if key in self.server_params.keys():
                        self.server_params[key] = value
                    elif key in self.client_params.keys():
                        self.client_params[key] = value
                    else:
                        raise ValueError("Invalid parameter for set")
                    self.tools.clear_console()
                    print("\nExample usage: set server_port = 9050")
                    print()
                    self.print_params()
                except Exception as e:
                    self.tools.clear_console()
                    print("\nExample usage: set server_port = 9050")
                    print()
                    self.print_params()
                    print(f"\nError occurred: {e}")

            elif choice in ('payload'):
                from remote_shell.libs.payload import Payload
                try:
                    Payload().create_payload(self.client_params)
                    print(f"Payload created successfully!")
                    input("\nPress any key to continue...")
                    self.tools.clear_console()
                    self.print_params()
                except Exception as e:
                    print(f"\nError occurred: {e}")
                    input("\nPress any key to continue...")
                    self.tools.clear_console()
                    self.print_params()

            elif choice in ('listen', 'start'):
                self.tools.clear_console()
                print(f"\nListening on port {self.server_params['server_port']}...\n\n")
                # listener server socket here
                _socket.Socket(self.server_params['server_host'], self.server_params['server_port'], self.server_params['server_buffersize']).listening()
            
            elif choice in ('params', 'param'):
                self.tools.clear_console()
                print("\nExample usage: set server_port = 9050")
                print()
                self.print_params()

            elif choice in ('help',):
                self.tools.clear_console()
                self.print_help()

            elif choice in ('clear',):
                self.tools.clear_console()

            elif choice in ('manual', 'man'):
                self.tools.clear_console()
                print("MANUAL")
            
            elif choice in ('exit', 'back'):
                self.tools.clear_console()
                break

            elif choice in ('quit', 'bye'):
                self.tools.clear_console()
                exit()